import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { EserComponent } from './eser/eser.component';
import { FormsModule } from "@angular/forms";
import  { UserServ } from "./shared/userServ";
import { NewUserGroupComponent } from './new-user-group/new-user-group.component';
import { ShowGroupComponent } from './show-group/show-group.component';
import {ServGroup} from "./shared/ServGroup";
import { GroupsComponent } from './groups/groups.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UsersComponent,
    EserComponent,
    NewUserGroupComponent,
    ShowGroupComponent,
    GroupsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserServ, ServGroup],
  bootstrap: [AppComponent]
})
export class AppModule { }
