import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EserComponent } from './eser.component';

describe('EserComponent', () => {
  let component: EserComponent;
  let fixture: ComponentFixture<EserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
