import { Component } from '@angular/core';
import {UserServ} from "../shared/userServ";
import {ServGroup} from "../shared/ServGroup";

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent {

  constructor(public usServ: ServGroup) {}

}
