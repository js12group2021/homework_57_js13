import {Component, ElementRef, ViewChild} from '@angular/core';
import {UserServ} from "../shared/userServ";
import {usForm} from "../shared/usForm";
import {UsGroup} from "../shared/usGroup";
import {ServGroup} from "../shared/ServGroup";

@Component({
  selector: 'app-new-user-group',
  templateUrl: './new-user-group.component.html',
  styleUrls: ['./new-user-group.component.css']
})
export class NewUserGroupComponent {

  @ViewChild('nameInputGr') nameInputGr!: ElementRef;

  constructor(public usServ: ServGroup) {}

  createGroup() {
    const name = this.nameInputGr.nativeElement.value;

    console.log(this.nameInputGr.nativeElement.value);
    const user = new UsGroup(name);
    // this.newUser.emit(user);

    this.usServ.NameGroup.push(user);
  }

}
