import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';
import {usForm} from "../shared/usForm";
import {UserServ} from "../shared/userServ";

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
})
export class NewUserComponent {
  contentEditable:boolean = false;
  // @Output() newUser = new EventEmitter<usForm>();
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('activeInput') IsActive!: ElementRef;

  constructor(public usServ: UserServ) {}
  createDish() {
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const act = this.IsActive.nativeElement.value;
    // console.log(this.IsActive.nativeElement.value);
    const user = new usForm(name, email, act);
    // this.newUser.emit(user);

    this.usServ.Users.push(user);
  }



}



