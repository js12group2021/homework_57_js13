export class usForm{
  constructor(
    public name:string,
    public email:string,
    public active:string
  ) {}

  public getActive(){
    this.active = 'Active';
  }

 public getPassive(){
    this.active = 'Passive';
  }
}
