import {Component, Input} from '@angular/core';

import {UsGroup} from "../shared/usGroup";

@Component({
  selector: 'app-show-group',
  templateUrl: './show-group.component.html',
  styleUrls: ['./show-group.component.css']
})
export class ShowGroupComponent {
  @Input() groupName!: UsGroup;

}
