import {Component, Input} from '@angular/core';
import {usForm} from "../shared/usForm";
import {UserServ} from "../shared/userServ";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent {

  // @Input() Users!: usForm[];

  constructor(public usServ: UserServ) {}

}
